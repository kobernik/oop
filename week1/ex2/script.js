let count = prompt('Введите кол-во переменных:');
let counts = [];

for (let i = 0; i < count; i++) {
  counts[i] = prompt('Введите переменную:');
}

for (let i = 0; i < count; i++) {
  console.log(`Переменная ${i + 1}: ${typeof counts[i]}`);
}

//если все типы переменных числовые, то выведет минимальное и максимальное
//значение из массива данных, иначе выведет NaN
console.log('Минимальное: ' + Math.min.apply(null, counts));
console.log('Максимальное: ' + Math.max.apply(null, counts));
