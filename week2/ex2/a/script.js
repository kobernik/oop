function Heron_func() {

  document.getElementById("btn").onclick = function() {
    const number_const = document.getElementById('number_const').value;
    document.getElementById("number_const").value = "";

    function x(n, a) {
      return ((1 / 2) * (n + a / n));
    }

    let a = number_const;
    let sqrt = 0;
    let n = 1;
    let e = 0.000000000000001;

    if (a < 0) {
      console.log('Это невозможно 0_0');
    } else {
      while (Math.abs(x(n, a) - n) > e) {
        sqrt = x(n, a);
        n = sqrt;
      }
      console.log(sqrt)
    }

  }

}
