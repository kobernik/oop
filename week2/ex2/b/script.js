function Eratosthenes_func() {

  document.getElementById("btn").onclick = function() {
    const number_const = document.getElementById('number_const').value;
    document.getElementById("number_const").value = "";

    let arr = [];
    let n = number_const;

    for (let i = 0; i < Number(n) + 1; i++) {
      arr[i] = i;
    }

    console.log('Изначальный массив: ' + arr)

    arr[1] = 0;
    let arr2 = [];
    let arr3 = [];

    if (n == 2) {
      arr2 = [2];
      arr3 = [2];
    } if (n >= 3) {
      arr2 = [2, 3];
      arr3 = [2, 3];
    }

    let j = 2;
    let koef = 1;

    for (i = 2; i < Number(n) + 1; i++) { 
      j = 2;

      while (j != arr[i]) {
        if (arr[i] % j != 0) {
          koef = 1;
        } else {
          koef = 0;
          break;
        }
        j += 1;
      }

      if (koef == 1) {
        arr2.push(arr[i]);
      }

    } 

    if (arr2.length > 3) {
      for (i = 3; i < arr2.length; i++) {
        if (arr2[i] != arr3[arr3.length - 1]){
          arr3.push(arr2[i]);
        }
      }
    }

    console.log('Простые числа: '+ arr3);

  }

}
