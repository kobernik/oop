function median_func() {

  document.getElementById("btn").onclick = function() {
    const count_const = document.getElementById('count_const').value;
    document.getElementById("count_const").value = "";

    function getRandomInt(max) {
      return Math.floor(Math.random() * (Number(max) + 1));
    }

    const bubbleSort = (coll) => {
      let stepsCount = coll.length - 1;
      // Объявляем переменную swapped, значение которой показывает,
      // был ли совершен обмен элементов во время перебора массива
      let swapped;
      // do..while цикл. Работает почти идентично while
      // Разница в проверке. Тут она делается не до выполнения тела, а после.
      // Такой цикл полезен там, где надо выполнить тело хотя бы раз в любом случае.
      do {
        swapped = false;
        // Перебираем массив и меняем местами элементы, если предыдущий
        // больше, чем следующий
        for (let i = 0; i < stepsCount; i += 1) {
          if (coll[i] > coll[i + 1]) {
            // temp – временная константа для хранения текущего элемента
            const temp = coll[i];
            coll[i] = coll[i + 1];
            coll[i + 1] = temp;
            // Если сработал if и была совершена перестановка,
            // присваиваем swapped значение true
            swapped = true;
          }
        }
        // Уменьшаем счетчик на 1, т.к. самый большой элемент уже находится
        // в конце массива
        stepsCount -= 1;
      } while (swapped); // продолжаем, пока swapped === true
    
      return coll;
    };

    let arr = [];
    let randomNum = 0;
    let varr = 1;

    while (arr.length < count_const) {
      randomNum = getRandomInt(count_const);

      for (let j = 0; j < arr.length; j++) {

        if (arr[j] == randomNum) {
          varr = 0;
          break;
        } else {
          varr = 1;
        }

      }

      if (varr == 1) {
        arr.push(randomNum);
      }

    }

    // Зачем так сложно заполнять массив?
    // Чтобы показать пункт сортировки перед нахождением медианы 

    console.log(`Исходный массив: ` + arr);

    bubbleSort(arr); // Кстати вот он :)

    console.log(`Отсортированный массив: ` + arr);

    if (arr.length % 2 == 0) {
      console.log(`Медиана равна ` + (arr[arr.length / 2 - 1] + arr[arr.length / 2]) / 2);;
    } else {
      console.log(`Медиана равна ` + arr[(arr.length - 1) / 2]);
    }

  }

}
