function countMaximum_func() {

  document.getElementById("btn").onclick = function() {
    const count_const = document.getElementById('count_const').value;
    document.getElementById("count_const").value = "";

    function getRandomInt(max) {
      return Math.floor(Math.random() * (Number(max) + 1));
    }

    let arr = [];

    for (let i = 0; i < count_const; i++) {
      arr[i] = getRandomInt(count_const);
    }

    let maximum = Math.max.apply(null, arr);

    let countMax = 0;

    for (i = 0; i < count_const; i++) {
      if (arr[i] == maximum) {
        countMax += 1;
      }
    }

    console.log(`Сгенерированный массив: ` + arr);
    console.log(`Кол-во максимального элемента ` + maximum + ` в данном массиве равно: ` + countMax + `.`);

  }

}
